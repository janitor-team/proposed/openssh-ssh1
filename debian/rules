#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS := hardening=+all

include /usr/share/dpkg/default.mk

# Uncomment this to turn on verbose mode.
# export DH_VERBOSE=1

# This has to be exported to make some magic below work.
export DH_OPTIONS

ifeq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
  PARALLEL :=
else
  PARALLEL := \
	-j$(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
endif

ifeq ($(DEB_BUILD_GNU_TYPE),$(DEB_HOST_GNU_TYPE))
  CC := gcc
  PKG_CONFIG = pkg-config
else
  CC := $(DEB_HOST_GNU_TYPE)-gcc
  PKG_CONFIG = $(DEB_HOST_GNU_TYPE)-pkg-config
endif

# Change the version string to reflect distribution
SSH_EXTRAVERSION := $(DEB_VENDOR)-$(shell echo '$(DEB_VERSION)' | sed -e 's/.*-//')

# Common path configuration.
confflags += --sysconfdir=/etc/ssh
confflags += --libexecdir=\$${prefix}/lib/openssh

# Common build options.
confflags += --disable-strip
confflags += --with-mantype=doc
confflags += --with-4in6

# The Hurd needs libcrypt for res_query et al.
ifeq ($(DEB_HOST_ARCH_OS),hurd)
confflags += --with-libs=-lcrypt
endif

# http://gcc.gnu.org/bugzilla/show_bug.cgi?id=60155
ifeq ($(DEB_HOST_ARCH),hppa)
confflags += --without-hardening
endif

confflags += --with-ssl-engine
ifeq ($(DEB_HOST_ARCH_OS),linux)
confflags += --with-selinux
endif

confflags += --with-xauth=/usr/bin/xauth

# Compiler flags.
cflags := $(CPPFLAGS) $(CFLAGS)
cflags += -DSSH_EXTRAVERSION=\"$(SSH_EXTRAVERSION)\"
confflags += --with-cflags='$(cflags)'

# Linker flags.
confflags += --with-ldflags='$(strip -Wl,--as-needed $(LDFLAGS))'

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -Bdebian/build-deb-ssh1 -- $(confflags) --with-ssh1

override_dh_auto_build:
	$(MAKE) -C debian/build-deb-ssh1 $(PARALLEL) SSH_PROGRAM='/usr/bin/ssh1' ASKPASS_PROGRAM='/usr/bin/ssh-askpass' ssh scp ssh-keygen ssh.1.out scp.1.out ssh-keygen.1.out

override_dh_auto_test:

override_dh_auto_clean:
	rm -rf debian/build-deb-ssh1

override_dh_auto_install:

# Tighten libssl dependencies to match the check in entropy.c.
override_dh_shlibdeps:
	dh_shlibdeps
	debian/adjust-openssl-dependencies
