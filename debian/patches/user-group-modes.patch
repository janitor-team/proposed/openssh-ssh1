From 49a66e89ce73edf82f99592242f0502bf2f683d9 Mon Sep 17 00:00:00 2001
From: Colin Watson <cjwatson@debian.org>
Date: Sun, 9 Feb 2014 16:09:58 +0000
Subject: Allow harmless group-writability

Allow secure files (~/.ssh/config, ~/.ssh/authorized_keys, etc.) to be
group-writable, provided that the group in question contains only the file's
owner.  Rejected upstream for IMO incorrect reasons (e.g. a misunderstanding
about the contents of gr->gr_mem).  Given that per-user groups and umask 002
are the default setup in Debian (for good reasons - this makes operating in
setgid directories with other groups much easier), we need to permit this by
default.

Bug: https://bugzilla.mindrot.org/show_bug.cgi?id=1060
Bug-Debian: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=314347
Last-Update: 2013-09-14

Patch-Name: user-group-modes.patch
---
 auth-rhosts.c |  6 ++---
 auth.c        |  9 +++----
 misc.c        | 69 ++++++++++++++++++++++++++++++++++++++++++++++++++-
 misc.h        |  2 ++
 platform.c    | 16 ------------
 readconf.c    |  3 +--
 ssh.1         |  2 ++
 ssh_config.5  |  2 ++
 8 files changed, 80 insertions(+), 29 deletions(-)

diff --git a/auth-rhosts.c b/auth-rhosts.c
index ecf956f06..4dccd5e6a 100644
--- a/auth-rhosts.c
+++ b/auth-rhosts.c
@@ -261,8 +261,7 @@ auth_rhosts2(struct passwd *pw, const char *client_user, const char *hostname,
 		return 0;
 	}
 	if (options.strict_modes &&
-	    ((st.st_uid != 0 && st.st_uid != pw->pw_uid) ||
-	    (st.st_mode & 022) != 0)) {
+	    !secure_permissions(&st, pw->pw_uid)) {
 		logit("Rhosts authentication refused for %.100s: "
 		    "bad ownership or modes for home directory.", pw->pw_name);
 		auth_debug_add("Rhosts authentication refused for %.100s: "
@@ -288,8 +287,7 @@ auth_rhosts2(struct passwd *pw, const char *client_user, const char *hostname,
 		 * allowing access to their account by anyone.
 		 */
 		if (options.strict_modes &&
-		    ((st.st_uid != 0 && st.st_uid != pw->pw_uid) ||
-		    (st.st_mode & 022) != 0)) {
+		    !secure_permissions(&st, pw->pw_uid)) {
 			logit("Rhosts authentication refused for %.100s: bad modes for %.200s",
 			    pw->pw_name, buf);
 			auth_debug_add("Bad file modes for %.200s", buf);
diff --git a/auth.c b/auth.c
index 6ee6116df..e5420dd61 100644
--- a/auth.c
+++ b/auth.c
@@ -443,8 +443,7 @@ check_key_in_hostfiles(struct passwd *pw, Key *key, const char *host,
 		user_hostfile = tilde_expand_filename(userfile, pw->pw_uid);
 		if (options.strict_modes &&
 		    (stat(user_hostfile, &st) == 0) &&
-		    ((st.st_uid != 0 && st.st_uid != pw->pw_uid) ||
-		    (st.st_mode & 022) != 0)) {
+		    !secure_permissions(&st, pw->pw_uid)) {
 			logit("Authentication refused for %.100s: "
 			    "bad owner or modes for %.200s",
 			    pw->pw_name, user_hostfile);
@@ -506,8 +505,7 @@ auth_secure_path(const char *name, struct stat *stp, const char *pw_dir,
 		snprintf(err, errlen, "%s is not a regular file", buf);
 		return -1;
 	}
-	if ((!platform_sys_dir_uid(stp->st_uid) && stp->st_uid != uid) ||
-	    (stp->st_mode & 022) != 0) {
+	if (!secure_permissions(stp, uid)) {
 		snprintf(err, errlen, "bad ownership or modes for file %s",
 		    buf);
 		return -1;
@@ -522,8 +520,7 @@ auth_secure_path(const char *name, struct stat *stp, const char *pw_dir,
 		strlcpy(buf, cp, sizeof(buf));
 
 		if (stat(buf, &st) < 0 ||
-		    (!platform_sys_dir_uid(st.st_uid) && st.st_uid != uid) ||
-		    (st.st_mode & 022) != 0) {
+		    !secure_permissions(&st, uid)) {
 			snprintf(err, errlen,
 			    "bad ownership or modes for directory %s", buf);
 			return -1;
diff --git a/misc.c b/misc.c
index cfd32729a..6e972f563 100644
--- a/misc.c
+++ b/misc.c
@@ -51,8 +51,9 @@
 #include <netdb.h>
 #ifdef HAVE_PATHS_H
 # include <paths.h>
-#include <pwd.h>
 #endif
+#include <pwd.h>
+#include <grp.h>
 #ifdef SSH_TUN_OPENBSD
 #include <net/if.h>
 #endif
@@ -61,6 +62,7 @@
 #include "misc.h"
 #include "log.h"
 #include "ssh.h"
+#include "platform.h"
 
 /* remove newline at end of string */
 char *
@@ -713,6 +715,71 @@ read_keyfile_line(FILE *f, const char *filename, char *buf, size_t bufsz,
 	return -1;
 }
 
+/*
+ * return 1 if the specified uid is a uid that may own a system directory
+ * otherwise 0.
+ */
+int
+platform_sys_dir_uid(uid_t uid)
+{
+	if (uid == 0)
+		return 1;
+#ifdef PLATFORM_SYS_DIR_UID
+	if (uid == PLATFORM_SYS_DIR_UID)
+		return 1;
+#endif
+	return 0;
+}
+
+int
+secure_permissions(struct stat *st, uid_t uid)
+{
+	if (!platform_sys_dir_uid(st->st_uid) && st->st_uid != uid)
+		return 0;
+	if ((st->st_mode & 002) != 0)
+		return 0;
+	if ((st->st_mode & 020) != 0) {
+		/* If the file is group-writable, the group in question must
+		 * have exactly one member, namely the file's owner.
+		 * (Zero-member groups are typically used by setgid
+		 * binaries, and are unlikely to be suitable.)
+		 */
+		struct passwd *pw;
+		struct group *gr;
+		int members = 0;
+
+		gr = getgrgid(st->st_gid);
+		if (!gr)
+			return 0;
+
+		/* Check primary group memberships. */
+		while ((pw = getpwent()) != NULL) {
+			if (pw->pw_gid == gr->gr_gid) {
+				++members;
+				if (pw->pw_uid != uid)
+					return 0;
+			}
+		}
+		endpwent();
+
+		pw = getpwuid(st->st_uid);
+		if (!pw)
+			return 0;
+
+		/* Check supplementary group memberships. */
+		if (gr->gr_mem[0]) {
+			++members;
+			if (strcmp(pw->pw_name, gr->gr_mem[0]) ||
+			    gr->gr_mem[1])
+				return 0;
+		}
+
+		if (!members)
+			return 0;
+	}
+	return 1;
+}
+
 int
 tun_open(int tun, int mode)
 {
diff --git a/misc.h b/misc.h
index c242f9011..8b223b55e 100644
--- a/misc.h
+++ b/misc.h
@@ -143,6 +143,8 @@ char	*read_passphrase(const char *, int);
 int	 ask_permission(const char *, ...) __attribute__((format(printf, 1, 2)));
 int	 read_keyfile_line(FILE *, const char *, char *, size_t, u_long *);
 
+int	 secure_permissions(struct stat *st, uid_t uid);
+
 #define MINIMUM(a, b)	(((a) < (b)) ? (a) : (b))
 #define MAXIMUM(a, b)	(((a) > (b)) ? (a) : (b))
 #define ROUNDUP(x, y)   ((((x)+((y)-1))/(y))*(y))
diff --git a/platform.c b/platform.c
index 973a63e40..18c7751de 100644
--- a/platform.c
+++ b/platform.c
@@ -197,19 +197,3 @@ platform_krb5_get_principal_name(const char *pw_name)
 	return NULL;
 #endif
 }
-
-/*
- * return 1 if the specified uid is a uid that may own a system directory
- * otherwise 0.
- */
-int
-platform_sys_dir_uid(uid_t uid)
-{
-	if (uid == 0)
-		return 1;
-#ifdef PLATFORM_SYS_DIR_UID
-	if (uid == PLATFORM_SYS_DIR_UID)
-		return 1;
-#endif
-	return 0;
-}
diff --git a/readconf.c b/readconf.c
index 09c0339b0..554268da8 100644
--- a/readconf.c
+++ b/readconf.c
@@ -1741,8 +1741,7 @@ read_config_file_depth(const char *filename, struct passwd *pw,
 
 		if (fstat(fileno(f), &sb) == -1)
 			fatal("fstat %s: %s", filename, strerror(errno));
-		if (((sb.st_uid != 0 && sb.st_uid != getuid()) ||
-		    (sb.st_mode & 022) != 0))
+		if (!secure_permissions(&sb, getuid()))
 			fatal("Bad owner or permissions on %s", filename);
 	}
 
diff --git a/ssh.1 b/ssh.1
index 4011c65aa..feef81a54 100644
--- a/ssh.1
+++ b/ssh.1
@@ -1484,6 +1484,8 @@ The file format and configuration options are described in
 .Xr ssh_config 5 .
 Because of the potential for abuse, this file must have strict permissions:
 read/write for the user, and not writable by others.
+It may be group-writable provided that the group in question contains only
+the user.
 .Pp
 .It Pa ~/.ssh/environment
 Contains additional definitions for environment variables; see
diff --git a/ssh_config.5 b/ssh_config.5
index 0b9fda02f..883f9e53d 100644
--- a/ssh_config.5
+++ b/ssh_config.5
@@ -1795,6 +1795,8 @@ The format of this file is described above.
 This file is used by the SSH client.
 Because of the potential for abuse, this file must have strict permissions:
 read/write for the user, and not accessible by others.
+It may be group-writable provided that the group in question contains only
+the user.
 .It Pa /etc/ssh/ssh_config
 Systemwide configuration file.
 This file provides defaults for those
